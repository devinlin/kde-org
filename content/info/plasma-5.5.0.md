---
version: "5.5.0"
title: "KDE Plasma 5.5.0, Feature Release"
errata:
    link: https://community.kde.org/Plasma/5.5_Errata
    name: 5.5 Errata
type: info/plasma5
---

This is a Feature release of KDE Plasma, featuring Plasma Desktop and
other essential software for your computer.  Details in the [Plasma 5.5.0 announcement](/announcements/plasma-5.5.0).
