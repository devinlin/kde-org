<table border="0" cellpadding="4" cellspacing="0">
<tr valign="top"><th align="left">Location</th><th align="left">Size</th><th align="left">MD5&nbsp;Sum</th></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.94/src/extragear-plasma-3.94.0.tar.bz2">extragear-plasma-3.94.0</a></td><td align="right">3.5MB</td><td><tt>a78e0d0d59dda8c452bf1352afe164ed</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.94/src/kdeaccessibility-3.94.0.tar.bz2">kdeaccessibility-3.94.0</a></td><td align="right">7.4MB</td><td><tt>599e8703e51b133d16e5f8c8ef9ced1b</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.94/src/kdeadmin-3.94.0.tar.bz2">kdeadmin-3.94.0</a></td><td align="right">1.4MB</td><td><tt>6c9aee495ce5062ae74fac08c25a078b</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.94/src/kdeartwork-3.94.0.tar.bz2">kdeartwork-3.94.0</a></td><td align="right">44MB</td><td><tt>d0414518233cfb4ccd181f963bc9029b</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.94/src/kdebase-3.94.0.tar.bz2">kdebase-3.94.0</a></td><td align="right">58MB</td><td><tt>0b21e7ca02d272a52847df7355a161d5</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.94/src/kdebase-workspace-3.94.0.tar.bz2">kdebase-workspace-3.94.0</a></td><td align="right">11MB</td><td><tt>9962b517113b2fc3c2a8a75b6f0763ff</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.94/src/kdebindings-3.94.0.tar.bz2">kdebindings-3.94.0</a></td><td align="right">2.2MB</td><td><tt>c2914ae12e821b729c887726fb789ea0</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.94/src/kdeedu-3.94.0.tar.bz2">kdeedu-3.94.0</a></td><td align="right">39MB</td><td><tt>31ed8ba5b5b56f8257576fbf85e8db92</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.94/src/kdegames-3.94.0.tar.bz2">kdegames-3.94.0</a></td><td align="right">23MB</td><td><tt>9de4a21ec974a2893074ac918b1d0d83</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.94/src/kdegraphics-3.94.0.tar.bz2">kdegraphics-3.94.0</a></td><td align="right">2.4MB</td><td><tt>60fd44b3da191c6b4d96f5a7b03f899a</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.94/src/kdelibs-3.94.0.tar.bz2">kdelibs-3.94.0</a></td><td align="right">9.0MB</td><td><tt>3fe36def8619fb35c3d81630e2fce8a4</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.94/src/kdemultimedia-3.94.0.tar.bz2">kdemultimedia-3.94.0</a></td><td align="right">1.0MB</td><td><tt>dee28e63370b8fcee92387c79abe2c10</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.94/src/kdenetwork-3.94.0.tar.bz2">kdenetwork-3.94.0</a></td><td align="right">6.3MB</td><td><tt>d5f0ee513f7d0cd162b59f7f39c8a858</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.94/src/kdepim-3.94.0.tar.bz2">kdepim-3.94.0</a></td><td align="right">13MB</td><td><tt>53a8872af212b6af4d2ada26d178d62b</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.94/src/kdepimlibs-3.94.0.tar.bz2">kdepimlibs-3.94.0</a></td><td align="right">1.7MB</td><td><tt>1165ffb93b8c55c1e560f1150de8650e</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.94/src/kdesdk-3.94.0.tar.bz2">kdesdk-3.94.0</a></td><td align="right">4.2MB</td><td><tt>24c8a301cd77ff6a0b788324c6a269ef</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.94/src/kdetoys-3.94.0.tar.bz2">kdetoys-3.94.0</a></td><td align="right">2.2MB</td><td><tt>6264433fb8bf8040a0e74aceca18cd81</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.94/src/kdeutils-3.94.0.tar.bz2">kdeutils-3.94.0</a></td><td align="right">2.3MB</td><td><tt>9db6db01b5c34dc92aad3afe4e8b01cd</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.94/src/koffice-1.9.94.tar.bz2">koffice-1.9.94</a></td><td align="right">61MB</td><td><tt>9ff2f7ec84a59506341d640b425bd0ee</tt></td></tr>
</table>
