---
aliases:
- ../announce-applications-15.08-beta
date: 2015-07-28
description: KDE Ships Applications 15.08 Beta.
layout: application
release: applications-15.07.80
title: KDE Ships Beta of KDE Applications 15.08
---

July 28, 2015. Today KDE released the beta of the new versions of KDE Applications. With dependency and feature freezes in place, the KDE team's focus is now on fixing bugs and further polishing.

With the various applications being based on KDE Frameworks 5, the KDE Applications 15.08 releases need a thorough testing in order to maintain and improve the quality and user experience. Actual users are critical to maintaining high KDE quality, because developers simply cannot test every possible configuration. We're counting on you to help find bugs early so they can be squashed before the final release. Please consider joining the team by installing the beta <a href='https://bugs.kde.org/'>and reporting any bugs</a>.