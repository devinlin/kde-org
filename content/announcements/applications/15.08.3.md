---
aliases:
- ../announce-applications-15.08.3
changelog: true
date: 2015-11-10
description: KDE Ships KDE Applications 15.08.3
layout: application
title: KDE Ships KDE Applications 15.08.3
version: 15.08.3
---

{{% i18n_var "November 10, 2015. Today KDE released the third stability update for <a href='%[1]s'>KDE Applications 15.08</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone." "../15.08.0" %}}

More than 20 recorded bugfixes include improvements to ark, dolphin, kdenlive, kdepim, kig, lokalize and umbrello.

{{% i18n_var "This release also includes Long Term Support version of KDE Development Platform %[1]s." "4.14.14" %}}