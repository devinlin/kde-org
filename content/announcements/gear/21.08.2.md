---
title: KDE Gear 21.08.2
date: 2021-10-07
hero_image: hero.png
images:
  - /announcements/gear/21.08.2/hero.png
sassFiles:
  - scss/components/swiper.scss
  - sass/gear.scss
jsFiles:
  - https://cdn.kde.org/aether-devel/version/kde-org/applications.3e16ae06.js
layout: gear
---

Over 120 individual programs plus dozens of programmer libraries and feature plugins are released simultaneously as part of KDE Gear.

Today they all get new bugfix source releases with updated translations, including:

+ kmail: Fix an  infinite SSL error dialog loop [Commit,](http://commits.kde.org/kdepim-runtime/edb7f6fdea2c9f44085a042531f56223f3fd8a2f) [Commit,](http://commits.kde.org/kimap/7ee241898bc225237b3475f6c109ffc55a4a74c0) [Commit,](http://commits.kde.org/ksmtp/fca378d55e223944ce512c9a8f8b789d1d3abcde) fixes [#423424](https://bugs.kde.org/423424)
+ konqueror: Make it compatible with KIO 5.86.0 and don't open every URL in a new window [Commit,](http://commits.kde.org/konqueror/8506c585594d9d0cfc0ebe8b869ca05ff7610fa7) fixes [#442636](https://bugs.kde.org/442636)
+ libksane: Fix multi page detection with certain scanners [Commit](http://commits.kde.org/libksane/a90b83faea5beaaab346ee6eff2f151581783beb)

Distro and app store packagers should update their application packages.

+ [21.08 release notes](https://community.kde.org/KDE_Gear/21.08_Release_notes) for information on tarballs and known issues.
+ [Package download wiki page](https://community.kde.org/Get_KDE_Software_on_Your_Linux_Distro)
+ [21.08.2 full changelog](/announcements/changelogs/gear/21.08.2/)
