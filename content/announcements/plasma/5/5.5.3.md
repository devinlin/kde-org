---
aliases:
- ../../plasma-5.5.3
changelog: 5.5.2-5.5.3
date: 2016-01-06
layout: plasma
figure:
  src: /announcements/plasma/5/5.5.0/plasma-5.5.png
asBugfix: true
---

- Fix icon hover effect breaking after Dashboard was used. <a href="http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=c35aa318d417b4fcb5b36324635de6b8e76cbaf1">Commit.</a>
- Use Oxygen sound instead of sound from kdelibs4's kde-runtime. <a href="http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=4cac96f7834b63ced12618475afd37644ab9e243">Commit.</a>
- [notifications] Refactor the screen handling code to fix 'Notification Settings wrong default display for notifications and notifaction position'. <a href="http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=0c6b354b7e22297544f1d37608d6fdcd777c4d52">Commit.</a> Code review <a href="https://git.reviewboard.kde.org/r/126408">#126408</a>. See bug <a href="https://bugs.kde.org/353966">#353966</a>. See bug <a href="https://bugs.kde.org/356461">#356461</a>
