---
aliases:
- ../changelog3_5_2to3_5_3
hidden: true
title: KDE 3.5.2 to KDE 3.5.3 Changelog
---

<p>
This page tries to present as much as possible the additions
and corrections that occurred in KDE between the 3.5.2 and 3.5.3 releases.
Nevertheless it should be considered as incomplete.
</p>

<!-- A list of bugs which are fixed but not build in...

-->

<!-- template for a bug
			<li> (<a href="http://bugs.kde.org/xx">bug #xx</a>)</li>
-->

<h3 id="arts">arts <font size="-2">[<a href="3_5_3/arts.txt">all SVN changes</a>]</font></h3>
<h3 id="kdelibs">kdelibs <font size="-2">[<a href="3_5_3/kdelibs.txt">all SVN changes</a>]</font></h3><ul>
	<li>KHTML
		<ul>
			<li>Don't crash when triple-clicking on a line that has :after or :before content, or anything that isn't in the DOM (<a href="http://bugs.kde.org/95319">bug #95319</a>)</li><!--CRASH-->
			<li>Implement nondeterministic CSS matching (<a href="http://bugs.kde.org/103285">bug #103285</a> and <a href="http://bugs.kde.org/101115">bug #101115</a>)</li>
			<li>Do not crash if an old target got destroyed. (<a href="http://bugs.kde.org/124554">bug #124554</a>)</li><!--CRASH-->
			<li>Fix static Y position of positioned objects in initial whitespace (3.5 regression). (<a href="http://bugs.kde.org/125185">bug #125185</a>)</li>
			<li>Fontrendering: A few percent speedup by caching font scalability info</li>
			<li>Konqueror will crash if a floated link uses :hover:before or :hover:after to create a positioned element. (<a href="http://bugs.kde.org/94997">bug #94997</a>)</li><!--CRASH-->
			<li>Text selection misbehaves for devanagari. (<a href="http://bugs.kde.org/101009">bug #101009</a>)</li>
			<li>Fix crash when hovering a HTML menu item (<a href="http://bugs.kde.org/117240">bug #117240</a>)</li><!--CRASH-->
			<li>Don't crash when null pointer passed to removeEventListener (<a href="http://bugs.kde.org/126100">bug #126100</a>)</li><!--CRASH-->
			<li>Prevent an infinite loop in parseToken() when dealing with broken HTML. (<a href="http://bugs.kde.org/126268">bug #126268</a>)</li>
			<li>If we saved creating a renderer at attach. Create it if we later need it. (<a href="http://bugs.kde.org/125715">bug #125715</a>)</li>
			<li>Don't allow to find text in password fields. (<a href="http://bugs.kde.org/126745">bug #126745</a>)</li>
			<li>Improve XHTML CSS styling</li>
			<li>Handle CSS restyling for all types of dynamic DOM changes and user interaction</li>
			<li>Only repaint the expossed background when scrolling. Optimizes a few slow pages with unoptimized X11 drivers. (bugs <a href="http://bugs.kde.org/125580">#125580</a> and <a href="http://bugs.kde.org/118806">#118806</a>)</li>
			<li>Speed-up painting and selections on pages with large tables.</li>
			<li>Match better the error-handling capabilities of Firefox with respect to DL/DT/DD elements. (bugs <a href="http://bugs.kde.org/121888">#121888</a>, <a href="http://bugs.kde.org/112069">#112069</a> and <a href="http://bugs.kde.org/109877">#109877</a>)</li>
			<li>Added missing sanity check which caused crashes in caret mode on pgup/pgdn
when there was no valid caret. (<a href="http://bugs.kde.org/120205">bug #120205</a>)</li>
			<li>Make sure copyBlt doesn't fail because of different depth. (<a href="http://bugs.kde.org/127189">bug #127189</a>)</li>
			<li>No other property depends on background-image, and applying it before the rest of the background short-hand breaks it under inheritance. (<a href="http://bugs.kde.org/127444">bug #127444</a>)</li>
		</ul>
	</li>
	<li>KIO
		<ul>
			<li>Add media kioslave support to KFileDialog (<a href="http://bugs.kde.org/105771">bug #105771</a> and <a href="http://bugs.kde.org/106077">bug #106077</a>)</li>
			<li>KPropertiesDialog first renames the file, then allows all plugins to save their changes.  (<a href="http://bugs.kde.org/105771">bug #105771</a>)</li>
			<li>Make KDirSelectDialog work with media:/ and similar. Fixes JuK.</li>
		</ul>
	</li>
	<li><a href="http://printing.kde.org/">KDEPrint</a>
		<ul>
			<li>Fix usage of private methods in <a href="http://www.cups.org/">CUPS</a> which broke KDEPrint with CUPS 1.2 (<a href="http://bugs.kde.org/124157">bug #124157</a>)</li>
			<li>Allow  adding multiple file to the to-be-printed list. (<a href="http://bugs.kde.org/114384">bug #114384</a>)</li>
			<li>No fax sent when special characters are included in the sender information (<a href="http://bugs.kde.org/106369">bug #106369</a>)</li>
			<li>Kdeprintfax ignores paper size setting when using Hylafax (<a href="http://bugs.kde.org/108066">bug #108066</a>)</li>
		</ul>
	</li>
	<li>KDE-UI
		<ul>
			<li>Use KConfigGroup to reset the config group automatically. (<a href="http://bugs.kde.org/124553">bug #124533</a>)</li>
			<li>Give a possibility to disable spellchecking. (<a href="http://bugs.kde.org/83423">bug #83423</a>, cf <a href="http://bugs.kde.org/109389">bug #109389</a>)</li>
		</ul>
	</li>
	<li>KIO-Slaves
		<ul>
			<li>KIO-HTTP
				<ul>
					<li>
					Keep the fragment when doing redirections. (the HTTP requests and redirections don't include fragments; it's a browser thing). (<a href="http://bugs.kde.org/124654">bug #124654</a>)
					</li>
				</ul>
			</li>
		</ul>
	</li>
</ul>

<h3 id="kdeaccessibility">kdeaccessibility <font size="-2">[<a href="3_5_3/kdeaccessibility.txt">all SVN changes</a>]</font></h3>
<h3 id="kdeaddons">kdeaddons <font size="-2">[<a href="3_5_3/kdeaddons.txt">all SVN changes</a>]</font></h3><ul>
	<li>domtreeviewer (Konqueror-Plugin)
		<ul>
			<li>Fix the crash when "reopening the dialog when it's already up" (<a href="http://bugs.kde.org/127405">bug #127405</a>)</li>
		</ul>
	</li>
</ul>

<h3 id="kdeadmin">kdeadmin <font size="-2">[<a href="3_5_3/kdeadmin.txt">all SVN changes</a>]</font></h3>
<h3 id="kdeartwork">kdeartwork <font size="-2">[<a href="3_5_3/kdeartwork.txt">all SVN changes</a>]</font></h3>
<h3 id="kdebase">kdebase <font size="-2">[<a href="3_5_3/kdebase.txt">all SVN changes</a>]</font></h3><ul>
	<li>General
		<ul>
			<li><b>New:</b> KDE startup reordered in order to improve startup time.</li>
			<li>KDialog: --title does not work in at least one case (<a href="http://bugs.kde.org/115723">bug #115723</a>)</li>
			<li>Don't show the screensaver when doing presentations (<a  href="http://websvn.kde.org/branches/KDE/3.5/kdebase/kdesktop/xautolock.cc?rev=542126&amp;view=log">commit</a>)</li>
			<li>If KDE shuts down while starting up (due to low battery status), saved session is lost (<a href="http://bugs.kde.org/100935">bug #100935</a>)</li><!--IMPORTANT-->
			<li>KSysGuard: Typos in the ksysguard PO file (<a href="http://bugs.kde.org/125729">bug #125729</a>)</li>
			<li>Random wallpapers don't change on every configuration change.</li>
		</ul>
	</li>
	<li>Kicker
		<ul>
			<li>Bookmarks: Look up konsole-bookmarks in the correct directory (<a href="http://websvn.kde.org/branches/KDE/3.5/kdebase/kicker/menuext/konsole/konsolebookmarkhandler.cpp?rev=527108&amp;view=log">commit</a>)</li>
			<li>Show/hide arrow not shown if all icons are hidden (<a href="http://bugs.kde.org/126374">bug #126374</a>)</li>
		</ul>
	</li>
	<li>KControl
		<ul>
			<li>Fix the broken priority setting. (<a href="http://bugs.kde.org/114910">bug #114910</a>)</li>
		</ul>
	</li>
	<li>KFind
		<ul>
			<li>Don't draw a separator, it causes a couple of ugly pixels. (<a href="http://bugs.kde.org/126742">bug #126742</a>)</li>
		</ul>
	</li>
	<li><a name="konsole" href="http://konsole.kde.org">Konsole</a>
		<ul>
			<li>Fix a possible crash on session save/logout (<a href="http://websvn.kde.org/branches/KDE/3.5/kdebase/konsole/konsole/konsole.cpp?rev=526028&amp;view=log">commit</a>)</li><!--CRASH-->
			<li>Numerous fixes due to the Coverity reports.</li>
			<li>Fix DCOP call setEncoding. (<a href="http://bugs.kde.org/120998">#120998</a>)</li>
			<li>kwrited - Add a 'Clear Messages' to the popup menu. (<a href="http://bugs.kde.org/52524">#52524</a>)</li>
			<li>Fix crash when user deletes all the sessions. (<a href="http://bugs.kde.org/121640">#121640</a>)</li>
			<li>Fix underline issue when there are ampersands in tab title. (<a href="http://bugs.kde.org/121782">#121782</a>)</li>
			<li>Remember the text color while moving the tab. (<a href="http://bugs.kde.org/125373">#125373</a>)</li>
			<li>Fix issue with tab/window titles not refreshing for the non-active session. (<a href="http://bugs.kde.org/125796">#125796</a>)</li>
			<li>Verify that the command is executable for '-e &lt;command&gt;.' (<a href="http://bugs.kde.org/125977">#125977</a>)</li>
			<li>Use the "DefaultSession" parameter in konsolerc. (<a href="http://bugs.kde.org/126557">#126557</a>)</li>
		</ul>
	</li>
	<li><a href="http://kate.kde.org">Kate</a>
		<ul>
			<li>Fix crash in View Difference (<a href="http://bugs.kde.org/123887">bug #123887</a>)</li><!--CRASH-->
			<li>Fix crash in external tools config when clicking the edit button. (cf. <a href="http://bugs.kde.org/121607">bug #121607</a>)</li><!--CRASH-->
		</ul>
	</li>
	<li><a href="http://www.konqueror.org/">Konqueror</a>
		<ul>
			<li>Fix multi selection and Show/Hide in toolbar. (<a href="http://bugs.kde.org/124308">#124308</a>)</li>
			<li>Fix QString/KURL confusion which probably caused encoding bugs with local paths on non-latin1 systems.</li>
			<li>Respect user-selected view mode when browsing removable media. (<a href="http://bugs.kde.org/108542">#108542</a>)</li>
			<li>Fixed detection of some Netscape plugins when packagers are built with new ld flags.</li>
		</ul>
	</li>
	<li>KWin
		<ul>
			<li>Don't force new windows to be at the beginning of the focus chain (windows with focus stealing prevention shouldn't be there).</li>
			<li>Don't deactivate the showing desktop state when a new window is shown that belongs to the desktop.(<a href="http://bugs.kde.org/113552">bug #113552</a>)</li>
			<li>KWin messes up ConfigureRequest:s, causing X crashes. (<a href="http://bugs.kde.org/124087">bug #124087</a>)</li><!--CRASH-->
			<li>Move minimized windows to the right place in focus chain. (<a href="http://bugs.kde.org/124807">bug #124807</a>)</li>
			<li>Separate focus chains are nice, but KDE-style Alt+Tab not limited to the current desktop (<a href="http://bugs.kde.org/124721">bug #124721</a>)</li>
			<li>Window switching (alt-tab) with focus (strictly) under or follow mouse does not switch all windows and newly focused window doesn't get focus (<a href="http://bugs.kde.org/123890">bug #123890</a>)</li>
			<li>Obey automatic un-hover option also when switching to a shaded window with alt+tab. (<a href="http://bugs.kde.org/126291">bug #126291</a>)</li>
			<li>Mouse cursor covers tooltips of KDE applications (<a href="http://bugs.kde.org/124355">bug #124355</a>) </li>
			<li>Fixed timestamp handling on 64bit systems, fixes focus problems when running KDE for a long time</li>
		</ul>
	</li>
</ul>

<h3 id="kdebindings">kdebindings <font size="-2">[<a href="3_5_3/kdebindings.txt">all SVN changes</a>]</font></h3>
<h3 id="kdeedu">kdeedu <font size="-2">[<a href="3_5_3/kdeedu.txt">all SVN changes</a>]</font></h3><ul>
	<li><a href="http://edu.kde.org/ktouch/index.php">KTouch</a>
		<ul>
			<li>Changed default lecture font from Courier to Monospace in KTouch and all lecture files (<a href="http://bugs.kde.org/108690">bug #108690</a>)</li>
			<li>Statistics are also stored for the mini-default level</li>
			<li>Statistics dialog has been rearranged</li>
			<li>Fixed freeze of appliation on click on "clear statistics" button (<a href="http://bugs.kde.org/119568">bug #119568</a>)</li>
			<li>Fixing several wrong keyboard-layouts (<a href="http://bugs.kde.org/119189">bug #119189</a>)</li>
			<li>Default keyboard is selected based on KDE locale (fixes <a href="http://bugs.kde.org/54379">bug #54379</a> and <a href="http://bugs.kde.org/99947">bug #99947</a>)</li>
			<li>Color scheme entries in the main menu will be shown without interpreting &amp; as automatic accelerator</li>
			<li>Slideline algorithm reworked and enhanced, cleared bugs related to improper cursor movement/size, wrong sliding etc. (fixes <a href="http://bugs.kde.org/54329">bug #54329</a> and <a href="http://bugs.kde.org/116838">bug #116838</a>)</li>
			<li>Trailing spaces are removed from lines when lecture is read (<a href="http://bugs.kde.org/107080">bug #107080</a>)</li>
			<li>KTouch has also seen some minor feature-improvements.
				<ul>
					<li>Configuration settings allow change of sliding widget height limit, useful for large screen resolutions (feature request: <a href="http://bugs.kde.org/76666">#76666)</a></li>
					<li>Line length limit removed, teacher text can now be as long as desired, wrong student line text is limited to size of teacher lengths + some offset</li>
					<li>New default mini level with a few more lines</li> 
					<li>Default keyboard is selected based on KDE locale (requests: <a href="http://bugs.kde.org/54379">#54379</a>, <a href="http://bugs.kde.org/99947">#99947</a>)</li>
					<li>Quick select lists of training lectures/keyboard layouts are now sorted alphabetically</li>
					<li>Added keyboard hiding feature in configuration (request: <a href="http://bugs.kde.org/104238">#104238</a>)</li>
					<li>Show language names for keyboards in addition to file names in quick selection menu</li>
					<li>Manual level change buttons can be disabled in automatic-level change mode (request: <a href="http://bugs.kde.org/121598">#121598</a>)</li>			
				</ul>
			</li>
		</ul>
	</li>
	<li><a href="http://edu.kde.org/kalzium/index.php">Kalzium</a>
	<ul>
		<li>Don't stop moving the KalziumTip if the mouse hovers it.</li>
	</ul>
	</li>
	<li><a href="http://edu.kde.org/kmplot/index.php">KmPlot</a>
		<ul>
			<li>Make the initial function color wrap around after 10 functions had been created.</li>
			<li>Use [-&pi;,&pi;] as the default interval for parametric equations instead of the current x-range.</li>
			<li>Fixed storage of small numbers (was using e.g. "1.22e-6", but as "e" is a constant, this was parsed as "(1.22*e)-6").</li>
			<li>Make sure range to numerically integrate over is tiled perfectly (<a href="http://bugs.kde.org/121051">bug #121051</a>).</li>
		</ul>
	</li>
</ul>

<h3 id="kdegames">kdegames <font size="-2">[<a href="3_5_3/kdegames.txt">all SVN changes</a>]</font></h3><ul>
	<li>KAsteroids
		<ul>
			<li>Missile goes through closer asteroids (<a href="http://bugs.kde.org/13388">#13388</a>)</li>
		</ul>
	</li>
</ul>

<h3 id="kdegraphics">kdegraphics <font size="-2">[<a href="3_5_3/kdegraphics.txt">all SVN changes</a>]</font></h3><ul>
	<li><a href="http://kpdf.kde.org">KPDF</a>
		<ul>
			<li>Fix bug parsing some TOCs (<a href="https://bugs.freedesktop.org/show_bug.cgi?id=6454">Poppler bug #6454</a>)</li>
			<li>Fix display of some JBIG2 files (<a href="https://bugs.freedesktop.org/show_bug.cgi?id=6500">Poppler bug #6500</a>)</li>
			<li>Enable print action when opening files using drag and drop (<a href="http://bugs.kde.org/126406">bug #126406</a>)</li>
			<li>Fix crash (<a href="http://bugs.kde.org/126760">bug #126760</a>)</li>
		</ul>
	</li>
	<li>KGhostView
		<ul>
			<li>Fix KGhostView .desktop files (<a href="http://bugs.kde.org/126488">bug #126488</a>)</li>
		</ul>
	</li>
	<li>Kuickshow
		<ul>
			<li>Fix remote browsing of http-urls (<a href="http://bugs.kde.org/127684">bug #127684</a>)</li>
			<li>Don't display non-existing files (<a href="http://bugs.kde.org/127685">bug #127685</a>)</li>
		</ul>
	</li>
</ul>

<h3 id="kdemultimedia">kdemultimedia <font size="-2">[<a href="3_5_3/kdemultimedia.txt">all SVN changes</a>]</font></h3><ul>
	<li>Changing the slider should enable the apply/reset buttons (<a href="http://bugs.kde.org/119962">bug #119962</a>)</li>
	<li>Fixed audiocd:/ to Ogg extraction when packagers are built with new ld flags.</li>
	<li><a href="http://developer.kde.org/~wheeler/juk.html">JuK</a>
		<ul>
			<li>Fix "JuK trying to add unknown category in File Renamer" (<a href="http://bugs.kde.org/124976">bug #124976</a>)</li>
		</ul>
	</li>
	<li>KAudioCreator
		<ul>
			<li>Fix regression causing renamed tracks to be disregarded when ripping/encoding</li>
		</ul>
	</li>
</ul>

<h3 id="kdenetwork">kdenetwork <font size="-2">[<a href="3_5_3/kdenetwork.txt">all SVN changes</a>]</font></h3><ul>
	<li><a href="http://kopete.kde.org">Kopete</a>
		<ul>
			<li>Fix automatic spellchecking when turning off rich text</li>
			<li>Prevent contacts from being added to a server side group called Top Level</li>
			<li>Message notification in contactlist (<a href="http://bugs.kde.org/126016">bug #126016</a>)</li>
			<li>Fix crash for AIM (<a href="http://lists.kde.org/?l=kde-core-devel&amp;m=114539794621241&amp;w=2">report</a>)</li><!--CRASH-->
			<li>Kopete is closing down all connections and reconnects after about each 1 minute (<a href="http://bugs.kde.org/101669">bug #101669</a>)</li>
			<li>Fix Kopete crash after change ICQ status to online. (<a href="http://bugs.kde.org/115772">bug #115772</a>)</li>
			<li>Fix crash in ICQ on disconnect due to connect elsewhere. (<a href="http://bugs.kde.org/122827">bug #122827</a>)</li>
		</ul>
	</li>
</ul>

<h3 id="kdepim">kdepim <font size="-2">[<a href="3_5_3/kdepim.txt">all SVN changes</a>]</font></h3><ul>
	<li>KAlarm
		<ul>
			<li><b>New:</b> Add DCOP calls and command line options to display the edit alarm dialog</li>
			<li><b>New:</b> Add Select All and Deselect actions &amp; shortcuts for import birthdays list</li>
			<li>Make system tray icon appear in non-KDE window managers (<a href="http://bugs.kde.org/123651">bug 123651</a>)</li>
			<li>Fix corruption of alarms displayed at logout and then deferred after login (<a href="http://bugs.kde.org/124508">bug 124508</a>)</li>
			<li>Fix erroneous adjustment of recurrence start date when saving alarm</li>
			<li>Fix crash when --play command line option is used, if compiled without aRts (<a href="http://bugs.kde.org/124835">bug 124835</a>)</li><!--CRASH-->
			<li>Don't show disabled alarms in system tray tooltip alarm list (<a href="http://bugs.kde.org/125675">bug 125675</a>)</li>
			<li>Fix reminder time not being saved in alarm templates (<a href="http://bugs.kde.org/127760">bug 127760</a>)</li>
		</ul>
	</li>
	<li>KArm
		<ul>
			<li>Make context menu work in the karm kontact plugin. (cf. bug <a href="http://bugs.kde.org/124190">#124190</a>)</li>
		</ul>
	</li>
	<li>CertManager
		<ul>
			<li>S/MIME support in QGPGME backend (bug <a href="http://bugs.kde.org/127389">#127389</a>)</li>
		</ul>
	</li>
	<li><a href="http://kmail.kde.org">KMail</a>
		<ul>
			<li><b>New:</b>: Implementing folder quick filing <a href="http://bugs.kde.org/113759">#113759</a>)</li>
			<li>Fix crash in disconnected IMAP (<a href="http://websvn.kde.org/branches/KDE/3.5/kdepim/kmail/kmfoldercachedimap.cpp?rev=526818&amp;view=log">commit</a>)</li><!--CRASH-->
			<li>Fixing the frequent loss of folder-settings (<a href="http://websvn.kde.org/branches/KDE/3.5/kdepim/kmail/kmfolder.cpp?rev=531267&amp;view=log">commit</a>)</li>
			<li>Fixes of several memory-leaks</li>
			<li>Message list view containing empty rows and causing crash when double clicking (<a href="http://bugs.kde.org/89549">#89549</a>)</li><!--CRASH-->
			<li>Cannot create new folders with IMAP using Dovecot-imapd (<a href="http://bugs.kde.org/121843">#121843</a>)</li>
			<li>Don't insert the separator after "Delete Folders" for system folders. (<a href="http://bugs.kde.org/125180">#125180</a>)</li>
			<li>Drag and drop of messages to composer fails on x64 systems (<a href="http://bugs.kde.org/125477">#125477</a>)</li>
			<li>Fix crash when trying to view source while the Message Preview Pane is hidden. (<a href="http://bugs.kde.org/85175">#85175</a>)</li><!--CRASH-->
			<li>Don't crash when IMAP mailbox modified externally (<a href="http://bugs.kde.org/126060">#126060</a>)</li><!--CRASH-->
			<li>Fix a syntax error in kmail_clamav.sh (<a href="http://bugs.kde.org/126529">#126529</a>)</li>
			<li>Don't crash when trying to "send again" a message that is not there. (<a href="http://bugs.kde.org/126571">#126571</a>)</li><!--CRASH-->
			<li>Properly initialize font buttons when the composer is started in HTML mode (<a href="http://websvn.kde.org/branches/KDE/3.5/kdepim/kmail/kmcomposewin.cpp?rev=536179&amp;view=log">commit</a>)</li>
			<li>Fix mixed language phrases when a non-English language is used for reply/forward phrases (<a href="http://bugs.kde.org/109533">bug 109533</a>)</li>
			<li>Improve detection of external references (<a href="http://bugs.kde.org/123520">bug 123520</a> )</li>
			<li>'Fallback character encoding' and 'detect missing attachments' settings now work properly (see <a href="http://bugs.kde.org/122571">bug 122571</a> and <a href="http://bugs.kde.org/124703">bug 124703</a> resp)</li>
			<li>Fix IMAP-crash after fresh startup and selecting mail (<a href="http://bugs.kde.org/126715">bug 126715</a>)</li>
			<li>Fix IMAP-crash while selecting mail in mail header view (<a href="http://bugs.kde.org/125723">bug 125723</a>)</li>
		</ul>
	</li>
	<li>IMAP
		<ul>
			<li>Only remove the / when it is not quoted - fixes wrong searches. (<a href="http://bugs.kde.org/123605">bug #123605</a>)</li>
		</ul>
	</li>
	<li><a href="http://korganizer.kde.org/">KOrganizer</a>
		<ul>
			<li>Fixed: "normal" events aren't displayed if they're covered/overlaped by floating, multiday events</li>
			<li>Show categories in list view (<a href="http://bugs.kde.org/123728">bug #123728</a>)</li>
		</ul>
	</li>
	<li><a href="http://pim.kde.org/components/kaddressbook.php">KAddressBook</a>
		<ul>
			<li>Make it possible to delete an URL again (<a href="http://bugs.kde.org/124063">bug #124063</a>)</li>
		</ul>
	</li>
	    <li><a href="http://akregator.sf.net/">Akregator</a>
		<ul>
			<li><b>New:</b> add author information to article header (in the article pane only)</li>
			<li>Prevent "Akregator is running" messages on startup (reset PID to -1 when closing akregator)</li>
			<li>Do not crash on startup when Combined View mode is activated (Happened only when experimental tagging is activated)</li>
			<li>Do not reset status of modified articles to "New". It's just too much noise for a tiny bit of signal. (<a href="http://bugs.kde.org/101907">bug #101907</a>)</li>
			<li>When iterating over articles using previous [unread] article, jump to the oldest article instead of newest when entering a new feed (<a href="http://bugs.kde.org/126316">bug #126316</a>)</li>
			<li>Disable apply button in config dialog when nothing was changed (<a href="http://bugs.kde.org/103820">bug #103820</a>)</li>
		</ul>
	</li>
	<li>Message subject unnecessarily HTML-encoded in download status popup (<a href="http://bugs.kde.org/127677">bug #127677</a>)</li>
</ul>

<h3 id="kdesdk">kdesdk <font size="-2">[<a href="3_5_3/kdesdk.txt">all SVN changes</a>]</font></h3><ul>
	<li><a href="http://uml.sourceforge.net">Umbrello</a>
		<ul>
			<li>Export all views (extension of <a href="http://bugs.kde.org/58809">#58809</a>)</li>
			<li>Static variables in java code are not marked static (<a href="http://bugs.kde.org/59190">#59190</a>)</li>
			<li>New alignment type &quot;distribute horizontally/vertically&quot; (extension of <a href="http://bugs.kde.org/67058">#67058</a>)</li>
			<li>Save autosave file to a more obvious place (<a href="http://bugs.kde.org/72019">#72019</a>)</li>
			<li>Usability and responsiveness improvements to code import (<a href="http://bugs.kde.org/93296">#93296</a>)</li>
			<li>Auto-complete in parameter properties dialog in class diagram (<a href="http://bugs.kde.org/104477">#104477</a>)</li>
			<li>Don't crash on application exit (<a href="http://bugs.kde.org/112092">#112092</a>)</li>
			<li>Memory problem when importing classes from C++ sources (<a href="http://bugs.kde.org/122668">#122668</a>)</li>
			<li>Nestable components and subsystems in Component diagram (<a href="http://bugs.kde.org/124326">#124326</a>)</li>
			<li>Switch back to arrow tool after association creation (<a href="http://bugs.debian.org/353344">debian/353344</a>)</li>
			<li>Code generation wizard fails at generating C++ implementation code (<a href="http://bugs.kde.org/126489">#126489</a>)</li>
			<li>Fix crash loading xmi file (<a href="http://bugs.kde.org/125331">#125331</a>, <a href="http://bugs.kde.org/126968">#126968</a>)</li>
			<li>User interaction with UMLWidget improvements (<a href="http://bugs.kde.org/126391">#126391</a>)</li>
			<li>Comments are cut short when generating PHP code (<a href="http://bugs.kde.org/126480">#126480</a>)</li>
			<li>Freeze on C++ class import (<a href="http://bugs.kde.org/126994">#126994</a>)</li>
			<li>Fix crash on importing Java 1.5 classes containing annotations (<a href="http://bugs.kde.org/127160">#127160</a>)</li>
		</ul>
	</li>
	<li><a href="http://kcachegrind.sourceforge.net/cgi-bin/show.cgi">KCacheGrind</a>
		<ul>
			<li>"Fix slow handling (loading, cycle detection, drawing the call graph) of
			profiles where functions have number of callers/callees in the thousands.
			These unusual profiles are typically produced by running callgrind on
			Valgrind itself (so called Valgrind self-hosting)."</li>
		</ul>
	</li>
</ul>

<h3 id="kdetoys">kdetoys <font size="-2">[<a href="3_5_3/kdetoys.txt">all SVN changes</a>]</font></h3><ul>
	<li>KTeaTime
		<ul>
			<li>Formatting of minutes and seconds is incorrect, has the space between seconds and "s" (<a href="http://bugs.kde.org/117932">bug #117932</a>)</li>
		</ul>
	</li>
</ul>

<h3 id="kdeutils">kdeutils <font size="-2">[<a href="3_5_3/kdeutils.txt">all SVN changes</a>]</font></h3><ul>
  <li>Ark
		<ul>
			<li>Fixed opening of 7-zip archives with recent versions of p7zip</li>
			<li>Prevent crashes caused by trying to compress files which were deleted (<a href="http://bugs.kde.org/113585">bug #113585</a>)</li><!--CRASH--> 
			<li>Fix opening archives with strange characters in the filename (<a href="http://bugs.kde.org/125788">bug #125788</a>)</li>
			<li>Enable again opening of .deb files (<a href="http://bugs.kde.org/126051">bug #126051</a>)</li>
			<li>Ark is not complaining when directories already exist (<a href="http://bugs.kde.org/108316">bug #108316</a>)</li>
			<li>The error window is now resizable (<a href="http://bugs.kde.org/105982">bug #105982</a>)</li>

    	</ul>
    </li>

  <li>KCalc
		<ul>
			<li>Fix crash (<a href="http://bugs.kde.org/125109">bug #125109</a>) </li><!--CRASH-->
		</ul>
	</li>
</ul>

<h3 id="kdevelop">kdevelop <font size="-2">[<a href="3_5_3/kdevelop.txt">all SVN changes</a>]</font></h3>
<h3 id="kdewebdev">kdewebdev <font size="-2">[<a href="3_5_3/kdewebdev.txt">all SVN changes</a>]</font></h3><ul>
<li><a href="http://quanta.kdewebdev.org">Quanta Plus</a>
<ul>
<li>another round of VPL fixes. Requires KDE 3.5.3 to work properly (<a href="http://bugs.kde.org/125434">bug #125434</a>)</li>
<li>don't crash when viewing remote files in VPL (<a href="http://bugs.kde.org/126314">bug #126314</a>)</li>
<li>silently ignore files from a project view that do not exist anymore (<a href="http://bugs.kde.org/126588">bug #126588</a>)</li>
<li>show a correct error message if a file does not exist (<a href="http://bugs.kde.org/126588">bug #126588</a>)</li>
<li>make the img and script tags standard compliant (<a href="http://bugs.kde.org/125596">bug #125596</a>)</li>
<li>don't loose important spaces when applying source indentation (<a href="http://bugs.kde.org/125213">bug #125213</a>)</li>
<li>add input button to the Forms toolbar (<a href="http://bugs.kde.org/125202">bug #125202</a>)</li>
</ul></li>
</ul>