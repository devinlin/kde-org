2008-03-27 20:34 +0000 [r790921]  binner

	* branches/KDE/4.0/kdebase/workspace/libs/plasma/corona.cpp: fix
	  painting of panel for new user

2008-03-27 21:39 +0000 [r790947]  binner

	* branches/KDE/4.0/kdebase/workspace/libs/plasma/view.cpp,
	  branches/KDE/4.0/kdebase/workspace/plasma/plasma/panelview.cpp,
	  branches/KDE/4.0/kdebase/workspace/libs/plasma/view.h: backport
	  r782388 and r782388 to fix at least the "relocate panel to
	  opposite edge" case

2008-03-29 21:09 +0000 [r791560]  mlaurent

	* branches/KDE/4.0/kdebase/workspace/kmenuedit/khotkeys.cpp:
	  Backport: fix hide/show configure shortcut

2008-03-29 21:29 +0000 [r791563]  mlaurent

	* branches/KDE/4.0/kdebase/workspace/kmenuedit/basictab.cpp:
	  Backport: fix use shortcut

2008-03-29 21:46 +0000 [r791572]  mlaurent

	* branches/KDE/4.0/kdebase/workspace/kmenuedit/basictab.cpp:
	  Backport: Reset shortcut when there is conflict

2008-03-31 16:16 +0000 [r792198-792196]  lunakl

	* branches/KDE/4.0/kdebase/runtime/kde4 (added),
	  branches/KDE/4.0/kdelibs/kdeui/kernel/kapplication.cpp,
	  branches/KDE/4.0/kdebase/runtime/CMakeLists.txt: Make KDE4
	  applications restart in saved session using a wrapper script
	  'kde4' if not running in a KDE4 session
	  (http://lists.kde.org/?t=120569055200005&r=1&w=2).

	* branches/KDE/4.0/kdebase/workspace/startkde.cmake: KDE3 cleanup
	  (http://lists.kde.org/?t=120569055200005&r=1&w=2).

2008-03-31 17:07 +0000 [r792215]  binner

	* branches/work/plasma-4.0-openSUSE/plasma/containments/panel/panel.cpp,
	  branches/KDE/4.0/kdebase/workspace/krunner/krunnerdialog.h,
	  branches/work/plasma-4.0-openSUSE/plasma/containments/panel/panel.h,
	  branches/work/plasma-4.0-openSUSE/libs/plasma/dialog.cpp,
	  branches/work/plasma-4.0-openSUSE/libs/plasma/dialog.h,
	  branches/KDE/4.0/kdebase/workspace/krunner/krunnerdialog.cpp:
	  backport r772465, recalculate all the margin sizes when the
	  plasma theme changes

2008-03-31 17:10 +0000 [r792217]  binner

	* branches/KDE/4.0/kdebase/workspace/krunner/krunnerdialog.h,
	  branches/KDE/4.0/kdebase/workspace/krunner/krunnerdialog.cpp:
	  revert last, wrong branch :-(

2008-03-31 17:59 +0000 [r792236]  pino

	* branches/KDE/4.0/kdebase/runtime/CMakeLists.txt: the 'kde4'
	  script is in the source dir

2008-04-02 03:09 +0000 [r792769]  tyrerj

	* branches/KDE/4.0/kdebase/workspace/plasma/desktoptheme/widgets/panel-background.svg:
	  BUG: 159981 Revision 792768 backported to Branch.

2008-04-02 11:48 +0000 [r792845]  dfaure

	* branches/KDE/4.0/kdebase/apps/lib/konq/konq_filetip.h,
	  branches/KDE/4.0/kdebase/apps/lib/konq/CMakeLists.txt,
	  branches/KDE/4.0/kdebase/apps/lib/konq/konq_menuactions.cpp,
	  branches/KDE/4.0/kdebase/apps/lib/konq/konq_popupmenu.cpp,
	  branches/KDE/4.0/kdebase/apps/lib/konq/konq_menuactions.h,
	  branches/KDE/4.0/kdebase/apps/lib/konq/tests/konqpopupmenutest.cpp:
	  Backports: fix memory leak, fix konq_popupmenu unit test, don't
	  install the currently unused-and-untested KonqFileTip, we want to
	  rewrite it (hmm, kdereview/kerry might have a problem)

2008-04-03 15:07 +0000 [r793299]  mueller

	* branches/KDE/4.0/kdebase/apps/konsole/src/ViewContainer.cpp:
	  merged 793298

2008-04-04 18:11 +0000 [r793651]  lunakl

	* branches/KDE/4.0/kdebase/workspace/kwin/main.cpp: Work around a
	  Qt4.4.0RC1 redrawing bug. BUG: 157659

2008-04-05 12:44 +0000 [r793846]  mlaurent

	* branches/KDE/4.0/kdebase/apps/keditbookmarks/actionsimpl.cpp:
	  Backport: Fix crash when we export bookmark with command line

2008-04-05 15:55 +0000 [r793920]  mkretz

	* branches/KDE/4.0/kdebase/runtime/phonon/xine/kcm/xineoptions.ui,
	  branches/KDE/4.0/kdebase/runtime/phonon/xine/kcm/xineoptions.cpp,
	  branches/KDE/4.0/kdebase/runtime/phonon/xine/kcm/xineoptions.h,
	  branches/KDE/4.0/kdebase/runtime/phonon/xine/xinestream.cpp:
	  Backport Patch by Kevin Kofler. Thanks a lot. fixes Phonon not to
	  crash if deinterlacing is not available

2008-04-07 09:03 +0000 [r794391]  dfaure

	* branches/KDE/4.0/kdebase/apps/lib/konq/konq_operations.cpp:
	  Deleting files is dangerous, but it's a user-initiated action in
	  the first place, so OK is fine as a default button, as discussed
	  on kde-core-devel. BUG: 156438

2008-04-07 12:51 +0000 [r794420]  dfaure

	* branches/KDE/4.0/kdebase/apps/konqueror/src/konqviewmanager.cpp:
	  Send a PartActivateEvent to the konqueror mainwindow; this is
	  useful for mainwindow-plugins, like the searchbar, which need to
	  know the active part.

2008-04-08 07:37 +0000 [r794641]  mlaurent

	* branches/KDE/4.0/kdebase/apps/konqueror/sidebar/web_module/web_module.cpp:
	  Fix signal/slot

2008-04-08 13:10 +0000 [r794729]  lunakl

	* branches/KDE/4.0/kdebase/workspace/ksmserver/startup.cpp,
	  branches/KDE/4.0/kdebase/workspace/ksmserver/server.cpp: Don't
	  run the wm saved in the session if this run there's a different
	  wm configured to be used. This prevents e.g. compiz taking over,
	  since it saves its command with --replace :(.

2008-04-08 19:14 +0000 [r794874]  ppenz

	* branches/KDE/4.0/kdebase/apps/dolphin/src/dolphinview.cpp,
	  branches/KDE/4.0/kdebase/apps/dolphin/src/dolphinview.h: Backport
	  of SVN commit 794872: Bypassed a Qt-issue where enabling the
	  menu-animation for QApplication emits a clicked() signal during
	  the context menu is open. I got no bug-number from the Trolltech
	  tasktracker yet, I'll update the number xxxxxx by the real number
	  as soon as I received the bug-number. CCBUG: 155574

2008-04-10 11:55 +0000 [r795467]  dfaure

	* branches/KDE/4.0/kdebase/apps/dolphin/src/dolphinpart.cpp:
	  Backport 795466: Load plugins at the end of the ctor, so that
	  everything is set up already.

2008-04-11 11:41 +0000 [r795774]  mueller

	* branches/KDE/4.0/kdebase/workspace/plasma/containments/desktop/desktop.cpp:
	  fix wallpaper not updating during config change

2008-04-13 09:16 +0000 [r796320]  mueller

	* branches/KDE/4.0/kdebase/runtime/drkonqi/main.cpp: merge -r795550
	  from /trunk

2008-04-13 15:32 +0000 [r796437]  uwolfer

	* branches/KDE/4.0/kdebase/workspace/klipper/urlgrabber.cpp:
	  Backport: SVN commit 795575 by uwolfer: Be more careful with
	  deleting QObject. Should hopefully fix the crash reported in
	  160634. Please try and comment here if there are still problems.
	  (I cannot reproduce this issue.) CCBUG:160634

2008-04-15 07:29 +0000 [r797245]  mlaurent

	* branches/KDE/4.0/kdebase/apps/konqueror/shellcmdplugin/kshellcmddialog.cpp,
	  branches/KDE/4.0/kdebase/apps/konqueror/shellcmdplugin/kshellcmddialog.h,
	  branches/KDE/4.0/kdebase/apps/konqueror/shellcmdplugin/CMakeLists.txt,
	  branches/KDE/4.0/kdebase/apps/konqueror/remoteencodingplugin/CMakeLists.txt,
	  branches/KDE/4.0/kdebase/apps/konqueror/remoteencodingplugin/kremoteencodingplugin.cpp,
	  branches/KDE/4.0/kdebase/apps/konqueror/shellcmdplugin/kshellcmdexecutor.cpp:
	  Backport from trunk. Now theses plugins can be shown

2008-04-15 07:34 +0000 [r797246]  mlaurent

	* branches/KDE/4.0/kdebase/apps/dolphin/src/dolphinpart.cpp,
	  branches/KDE/4.0/kdebase/apps/dolphin/src/dolphinpart.h: Backport
	  it too necessary to use konqueror plugins

2008-04-15 16:09 +0000 [r797365]  gyurco

	* branches/KDE/4.0/kdebase/apps/nsplugins/viewer/nsplugin.cpp:
	  Backport fix for #160413 CCBUG:160413

2008-04-17 01:32 +0000 [r797912]  sebsauer

	* branches/KDE/4.0/kdebase/workspace/ksplash/kcm/installer.cpp:
	  installing themes doesn't workanyway in KDE 4.0.x plus it may
	  prevent unwanted effects (not sure if they are around in 4.0.x
	  too but in trunk r796690 was fixing installation to the
	  root-dir). So, better be safe then sorry ;)

2008-04-17 01:36 +0000 [r797913]  sebsauer

	* branches/KDE/4.0/kdebase/workspace/ksplash/kcm/installer.cpp: and
	  what may happen to someone if he missed sleep for more then 48h
	  is, that he patches something wrong. hmpf, time for more coffee
	  :)

2008-04-18 12:58 +0000 [r798504]  lunakl

	* branches/KDE/4.0/kdebase/workspace/kwin/workspace.h,
	  branches/KDE/4.0/kdebase/workspace/kwin/geometry.cpp,
	  branches/KDE/4.0/kdebase/workspace/kwin/workspace.cpp: Use
	  QVector instead of plain-C-error-prone-memory-leaking arrays.

2008-04-18 14:04 +0000 [r798524]  lunakl

	* branches/KDE/4.0/kdebase/workspace/kwin/utils.cpp: Don't crash
	  when XGrabKeyboard() fails for some reason.

2008-04-18 14:11 +0000 [r798528]  lunakl

	* branches/KDE/4.0/kdebase/workspace/ksplash/simple/main.cpp: Avoid
	  a silly infinite loop. CCMAIL: mueller@kde.org

2008-04-18 15:48 +0000 [r798568]  ogoffart

	* branches/KDE/4.0/kdebase/runtime/knotify/knotifyconfig.cpp:
	  Backport: cache the KSharedConfigPtr, because parsing the config
	  file is slow. BUG: 156346

2008-04-18 15:50 +0000 [r798570]  lunakl

	* branches/KDE/4.0/kdebase/workspace/kwin/client.h,
	  branches/KDE/4.0/kdebase/workspace/kwin/manage.cpp,
	  branches/KDE/4.0/kdebase/workspace/kwin/group.cpp: Don't initialy
	  minimize a window if it has some main window open, but it was
	  internall optimized away (e.g. one group transient plasma dialog
	  open, minimized, open another group transient dialog from
	  plasma).

2008-04-18 16:07 +0000 [r798590]  mlaurent

	* branches/KDE/4.0/kdebase/workspace/kcontrol/kdm/kdm-theme.cpp:
	  Backport: Add parent to knewstuff2 dialog

2008-04-18 16:17 +0000 [r798597]  mlaurent

	* branches/KDE/4.0/kdebase/workspace/kcontrol/kdm/kdm.knsrc:
	  Backport: don't add comment in same line as entry

2008-04-18 17:14 +0000 [r798613]  lunakl

	* branches/KDE/4.0/kdebase/workspace/ksplash/ksplashx/splash.cpp:
	  Don't exit when an image is outside the screen, may happen e.g.
	  with unscaled images and too small resolution.

2008-04-23 14:36 +0000 [r800166]  lunakl

	* branches/KDE/4.0/kdebase/workspace/kscreensaver/libkscreensaver/main.cpp:
	  Hmm, funny, two widgets with one winId. Make the demo mode
	  actually quit.

2008-04-23 15:56 +0000 [r800176]  kkofler

	* branches/KDE/4.0/kdebase/workspace/ksmserver/shutdowndlg.cpp:
	  Unbreak suspend/hibernate on logout (rh#442559, patch by Than
	  Ngo). (backport rev 800175 from trunk)

2008-04-23 17:09 +0000 [r800208]  lunakl

	* branches/KDE/4.0/kdebase/workspace/kscreensaver/libkscreensaver/main.cpp:
	  Fix basic (e.g. Banner) screensavers painting.

2008-04-23 17:35 +0000 [r800221]  lunakl

	* branches/KDE/4.0/kdebase/workspace/kscreensaver/libkscreensaver/main.cpp:
	  Better fix.

2008-04-23 18:17 +0000 [r800238]  uwolfer

	* branches/KDE/4.0/kdebase/workspace/ksystraycmd/ksystraycmd.cpp,
	  branches/KDE/4.0/kdebase/workspace/ksystraycmd/ksystraycmd.h:
	  Backport: SVN commit 800236 by uwolfer: * fix mouse press events
	  (patch by Sergey Saukh <thelich yandex ru>) * fix icons

2008-04-23 20:10 +0000 [r800277]  lunakl

	* branches/KDE/4.0/kdebase/workspace/kwin/layers.cpp: Off-by-one
	  errors in stacking unmanaged windows.

2008-04-24 12:54 +0000 [r800582]  lunakl

	* branches/KDE/4.0/kdebase/workspace/kwin/composite.cpp,
	  branches/KDE/4.0/kdebase/workspace/kwin/workspace.h,
	  branches/KDE/4.0/kdebase/workspace/kwin/events.cpp,
	  branches/KDE/4.0/kdebase/workspace/kwin/workspace.cpp,
	  branches/KDE/4.0/kdebase/workspace/kwin/layers.cpp: Fix handling
	  of stacking order of unmanaged windows. Could maybe use a little
	  bit more of optimization. BUG: 157878

2008-04-25 13:54 +0000 [r801029]  lunakl

	* branches/KDE/4.0/kdebase/runtime/kdesu/kdesu/kdesu.cpp: Follow
	  defKeep (kdelibs/kdesu/defaults.h).

2008-04-25 17:23 +0000 [r801103]  sitter

	* branches/KDE/4.0/kdebase/workspace/kdm/kfrontend/pics/kdelogo-crystal.png
	  (removed),
	  branches/KDE/4.0/kdebase/workspace/kdm/kfrontend/pics/kdelogo.png:
	  * replace old KDE logo with Oxygen one

2008-04-26 08:47 +0000 [r801275]  pino

	* branches/KDE/4.0/kdebase/workspace/kdm/kfrontend/pics/CMakeLists.txt:
	  kdelogo-crystal.png was removed, so do not install it anymore

2008-04-27 22:14 +0000 [r801845]  dfaure

	* branches/KDE/4.0/kdebase/apps/konsole/src/SessionManager.cpp: Fix
	  infinite loop when a profile somehow points to itself as parent
	  -- not sure how I ended up with that situation after editing
	  profiles, but Shell.profile said
	  Parent=/home/dfaure/.kde4/share/apps/konsole/Shell.profile, i.e.
	  itself.

2008-04-29 09:14 +0000 [r802406]  lunakl

	* branches/KDE/4.0/kdebase/workspace/kwin/client.cpp: Command-line
	  args don't need explicit encoding.

2008-04-30 08:47 +0000 [r802662]  lunakl

	* branches/KDE/4.0/kdebase/workspace/ksplash/ksplashx/splash.cpp,
	  branches/KDE/4.0/kdebase/workspace/ksplash/simple/main.cpp: Make
	  the splash timeout longer than just 1 minute. It now stays only
	  as long as Plasma is not ready (and that not changing means quite
	  a broken setup anyway), and in some rare cases the desktop setup
	  may really take longer than just 1 minute.

