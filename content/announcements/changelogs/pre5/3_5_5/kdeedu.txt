2006-08-01 15:27 +0000 [r568552]  saxton

	* branches/KDE/3.5/kdeedu/khangman/khangman/khangman.cpp: Backport
	  568550.

2006-08-02 21:51 +0000 [r569085]  dannya

	* branches/KDE/3.5/kdeedu/kanagram/data/worldcapitals.kvtml:
	  capital of Turkey is Ankara, not Istanbul. Thanks for the report.
	  CCMAIL: orkan_okyay@yahoo.com

2006-08-09 10:30 +0000 [r571342]  mueller

	* trunk/extragear/utils/Makefile.am.in,
	  trunk/extragear/graphics/Makefile.am.in,
	  branches/KDE/3.5/kdeaddons/Makefile.am.in,
	  branches/KDE/3.5/kdebase/Makefile.am.in,
	  branches/KDE/3.5/kdeedu/Makefile.am.in,
	  branches/KDE/3.5/kdesdk/Makefile.am.in,
	  branches/koffice/1.5/koffice/Makefile.am.in,
	  branches/KDE/3.5/kdepim/Makefile.am.in,
	  branches/KDE/3.5/kdeaccessibility/Makefile.am.in,
	  branches/KDE/3.5/kdeadmin/Makefile.am.in,
	  branches/KDE/3.5/kdenetwork/Makefile.am.in,
	  branches/KDE/3.5/kdelibs/Makefile.am.in,
	  branches/KDE/3.5/kdeartwork/Makefile.am.in,
	  branches/arts/1.5/arts/Makefile.am.in,
	  trunk/extragear/pim/Makefile.am.in,
	  branches/KDE/3.5/kdemultimedia/Makefile.am.in,
	  branches/KDE/3.5/kdegames/Makefile.am.in,
	  trunk/playground/sysadmin/Makefile.am.in,
	  trunk/extragear/network/Makefile.am.in,
	  trunk/extragear/libs/Makefile.am.in,
	  branches/KDE/3.5/kdebindings/Makefile.am.in,
	  branches/KDE/3.5/kdetoys/Makefile.am.in,
	  trunk/extragear/multimedia/Makefile.am.in,
	  branches/KDE/3.5/kdeutils/Makefile.am.in,
	  branches/KDE/3.5/kdegraphics/Makefile.am.in: update automake
	  version

2006-08-14 17:39 +0000 [r573057]  dannya

	* branches/KDE/3.5/kdeedu/doc/kanagram/index.docbook,
	  branches/KDE/3.5/kdeedu/doc/blinken/index.docbook: fix
	  documentation errors

2006-08-14 17:46 +0000 [r573061]  dannya

	* branches/KDE/3.5/kdeedu/doc/kalzium/index.docbook: fix some more
	  documentation errors

2006-08-14 17:57 +0000 [r573063]  dannya

	* branches/KDE/3.5/kdeedu/doc/kbruch/index.docbook,
	  branches/KDE/3.5/kdeedu/doc/klatin/index.docbook: fix some more
	  documentation errors

2006-08-15 15:49 +0000 [r573292]  mlaurent

	* branches/KDE/3.5/kdeedu/ktouch/sounds/Makefile.am: Fix install
	  wav file (found when I cleaned CMakefiles.txt into kde4.0)

2006-08-19 10:41 +0000 [r574538]  dannya

	* branches/KDE/3.5/kdeedu/doc/ktouch/index.docbook: fix ktouch doc
	  errors

2006-08-19 10:49 +0000 [r574540]  dannya

	* branches/KDE/3.5/kdeedu/doc/kalzium/index.docbook,
	  branches/KDE/3.5/kdeedu/doc/kmplot/credits.docbook,
	  branches/KDE/3.5/kdeedu/doc/kvoctrain/index.docbook,
	  branches/KDE/3.5/kdeedu/doc/kmplot/index.docbook: fix more doc
	  errors

2006-08-19 11:23 +0000 [r574544]  dannya

	* branches/KDE/3.5/kdeedu/doc/kstars/index.docbook,
	  branches/KDE/3.5/kdeedu/doc/kpercentage/index.docbook,
	  branches/KDE/3.5/kdeedu/doc/kiten/index.docbook,
	  branches/KDE/3.5/kdeedu/doc/klettres/index.docbook,
	  branches/KDE/3.5/kdeedu/doc/kbruch/index.docbook,
	  branches/KDE/3.5/kdeedu/doc/kpercentage/credits.docbook,
	  branches/KDE/3.5/kdeedu/doc/kgeography/index.docbook,
	  branches/KDE/3.5/kdeedu/doc/kvoctrain/index.docbook,
	  branches/KDE/3.5/kdeedu/doc/klatin/index.docbook: fix lots of doc
	  errors

2006-08-20 12:22 +0000 [r574855]  dannya

	* branches/KDE/3.5/kdeedu/doc/kverbos/index.docbook,
	  branches/KDE/3.5/kdeedu/doc/ktouch/index.docbook,
	  branches/KDE/3.5/kdeedu/doc/kgeography/index.docbook,
	  branches/KDE/3.5/kdeedu/doc/kvoctrain/index.docbook: fixing more
	  doc errors

2006-08-21 15:05 +0000 [r575432]  jriddell

	* branches/KDE/3.5/kdeedu/COPYING-DOCS (added): Add FDL licence for
	  documentation

2006-08-22 02:39 +0000 [r575770]  gwright

	* branches/KDE/3.5/kdeedu/klatin/klatin/data/vocab/sk/QR.kvtml
	  (added),
	  branches/KDE/3.5/kdeedu/klatin/klatin/data/vocab/sk/S.kvtml
	  (added),
	  branches/KDE/3.5/kdeedu/klatin/klatin/data/vocab/sk/D.kvtml
	  (added), branches/KDE/3.5/kdeedu/klatin/klatin/data/vocab/sk
	  (added),
	  branches/KDE/3.5/kdeedu/klatin/klatin/data/vocab/sk/BC.kvtml
	  (added),
	  branches/KDE/3.5/kdeedu/klatin/klatin/data/vocab/sk/V.kvtml
	  (added),
	  branches/KDE/3.5/kdeedu/klatin/klatin/data/vocab/sk/I2L.kvtml
	  (added),
	  branches/KDE/3.5/kdeedu/klatin/klatin/data/vocab/sk/GHI1.kvtml
	  (added),
	  branches/KDE/3.5/kdeedu/klatin/klatin/data/vocab/sk/TU.kvtml
	  (added),
	  branches/KDE/3.5/kdeedu/klatin/klatin/data/vocab/sk/EF.kvtml
	  (added),
	  branches/KDE/3.5/kdeedu/klatin/klatin/data/vocab/sk/M.kvtml
	  (added),
	  branches/KDE/3.5/kdeedu/klatin/klatin/data/vocab/sk/N.kvtml
	  (added),
	  branches/KDE/3.5/kdeedu/klatin/klatin/data/vocab/sk/OP1.kvtml
	  (added),
	  branches/KDE/3.5/kdeedu/klatin/klatin/data/vocab/sk/A.kvtml
	  (added),
	  branches/KDE/3.5/kdeedu/klatin/klatin/data/vocab/sk/P2.kvtml
	  (added): Slovakian vocabulary files CCMAIL: jose1711@gmail.com

2006-08-23 14:07 +0000 [r576223]  binner

	* branches/KDE/3.5/kdeedu/doc/klettres/index.docbook: fix build

2006-08-30 17:58 +0000 [r578976]  dannya

	* branches/KDE/3.5/kdeedu/doc/kverbos/index.docbook: make EBN happy

2006-08-30 18:06 +0000 [r578977]  dannya

	* branches/KDE/3.5/kdeedu/doc/kvoctrain/index.docbook: make EBN
	  happy

2006-08-31 07:08 +0000 [r579140]  ingwa

	* branches/KDE/3.5/kdeedu/kalzium/src/data/data.xml: Fix bug
	  133297: Misspelling of Scheele BUGS: 133297 Hi Magnus! Is all
	  well in Linköping? Another thing: In the data the first name is
	  given as K. W. in all instances, but Magnus says it is C. W.
	  Which is the correct form?

2006-09-02 07:47 +0000 [r579968-579966]  mueller

	* branches/KDE/3.5/kdeedu/kvoctrain/kvoctrain/query-dialogs/QueryDlgBase.h,
	  branches/KDE/3.5/kdeedu/kvoctrain/kvoctrain/Makefile.am,
	  branches/KDE/3.5/kdeedu/kvoctrain/kvoctrain/kvt-core/QueryManager.cpp,
	  branches/KDE/3.5/kdeedu/kvoctrain/kvoctrain/query-dialogs/Makefile.am,
	  branches/KDE/3.5/kdeedu/kvoctrain/kvoctrain/kvt-core/QueryManager.h:
	  trying to add missing dependencies. lets see if unsermake is
	  clever enough for that

	* branches/KDE/3.5/kdeedu/ktouch/keyboards/hungarian.ktouch.xml
	  (added), branches/KDE/3.5/kdeedu/ktouch/keyboards/Makefile.am:
	  add hu

	* branches/KDE/3.5/kdeedu/kstars/kstars/indi/indidrivermain.c: add
	  protection against setuid failing

2006-09-02 11:36 +0000 [r580013]  ingwa

	* branches/KDE/3.5/kdeedu/kalzium/src/data/data.xml: K. W. Scheele
	  -> C. W. Scheele, as this is the correct spelling. The same
	  change is also done for KDE4, but not in our SVN, since the data
	  is taken from outside. CCBUG: 133297

2006-09-03 20:52 +0000 [r580546]  aacid

	* branches/KDE/3.5/kdeedu/kgeography/data/world.png,
	  branches/KDE/3.5/kdeedu/kgeography/data/europe.kgm,
	  branches/KDE/3.5/kdeedu/kgeography/data/flags/montenegro.png
	  (added),
	  branches/KDE/3.5/kdeedu/kgeography/data/flags/serbiamontenegro_3x2.png
	  (removed),
	  branches/KDE/3.5/kdeedu/kgeography/data/flags/svg/serbiamontenegro_3x2.svgz
	  (removed), branches/KDE/3.5/kdeedu/kgeography/data/europe.png,
	  branches/KDE/3.5/kdeedu/kgeography/data/flags/serbia.png (added),
	  branches/KDE/3.5/kdeedu/kgeography/data/world.kgm,
	  branches/KDE/3.5/kdeedu/kgeography/data/flags/Makefile.am,
	  branches/KDE/3.5/kdeedu/kgeography/data/flags/svg/serbia.svgz
	  (added),
	  branches/KDE/3.5/kdeedu/kgeography/src/mapsdatatranslation.cpp:
	  Fix bug 133499 also on stable branch. Adds a few messages to
	  translate. CCMAIL: kde-i18n-doc@kde.org

2006-09-05 01:31 +0000 [r581020]  tyrerj

	* branches/KDE/3.5/kdeedu/kig/configure.in.in: Adding:
	  "-lboost_python-gcc-mt" to the search list for BoostPython so
	  that it will work with version 1.33.x. Note that it appears that
	  changes to Boost will mean that the version number won't be
	  needed for this to work with future version (I hope).

2006-09-09 15:13 +0000 [r582494]  aacid

	* branches/KDE/3.5/kdeedu/kanagram/src/keduvockvtmlwriter.cpp: it
	  seems we want the input to be UTF8, so make sure we write as UTF8
	  CCMAIL: joshuakeel@gmail.com It seems the KDE4 code in kdeeducore
	  has the same problem, do you guys want me to apply the fix there
	  too? CCMAIL: peter@peterandlinda.com CCMAIL: eric@erixpage.com

2006-09-09 19:37 +0000 [r582569]  pino

	* branches/KDE/3.5/kdeedu/kig/scripting/script_mode.cc: correctly
	  sets the document as modified when editing a script

2006-09-19 12:18 +0000 [r586382]  harris

	* branches/KDE/3.5/kdeedu/kstars/kstars/main.cpp: Increment version
	  number for KDE-3.5.5

2006-09-19 17:30 +0000 [r586461]  aacid

	* branches/KDE/3.5/kdeedu/kgeography/src/main.cpp: increase version

2006-09-24 19:42 +0000 [r588062]  ewoerner

	* branches/KDE/3.5/kdeedu/kalzium/src/main.cpp,
	  branches/KDE/3.5/kdeedu/kalzium/src/detailinfodlg.cpp: Fix the
	  spectrum bug in 3.5 branch, version number++ BUGS:134086

2006-09-28 07:17 +0000 [r589490]  annma

	* branches/KDE/3.5/kdeedu/kiten/kiten.desktop: add a comment
	  CCBUG=121580

2006-09-29 02:31 +0000 [r589956]  harris

	* branches/KDE/3.5/kdeedu/kstars/kstars/kstarsdata.cpp,
	  branches/KDE/3.5/kdeedu/kstars/kstars/skymapdraw.cpp: Fixing bug
	  #134824 (Position angle of deep sky objects changes with
	  pointing). A 90-degree adjustment to the position angles was
	  being applied incorrectly. Thanks for the report. Also fixing a
	  similar issue in trunk. CCMAIL: kstars-devel@kde.org BUG: 134824

2006-09-29 07:04 +0000 [r590014]  annma

	* branches/KDE/3.5/kdeedu/kiten/kiten.desktop: sorry about that - I
	  did not realize this was a new string while in message freeze my
	  bad!

2006-10-02 11:07 +0000 [r591328]  coolo

	* branches/KDE/3.5/kdeedu/kdeedu.lsm: updates for 3.5.5

