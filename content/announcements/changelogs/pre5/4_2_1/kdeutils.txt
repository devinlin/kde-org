------------------------------------------------------------------------
r915025 | kossebau | 2009-01-22 12:02:18 +0000 (Thu, 22 Jan 2009) | 2 lines

backport of 915024: changed: remove Print from toolbar, add Save As

------------------------------------------------------------------------
r916066 | scripty | 2009-01-24 13:13:52 +0000 (Sat, 24 Jan 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r916160 | kossebau | 2009-01-24 18:38:37 +0000 (Sat, 24 Jan 2009) | 2 lines

backport  of 916156: fixed: enable filtering only if view is not readonly

------------------------------------------------------------------------
r916163 | kossebau | 2009-01-24 18:46:15 +0000 (Sat, 24 Jan 2009) | 1 line

changed: version is now 0.2.1
------------------------------------------------------------------------
r916229 | kossebau | 2009-01-24 21:46:35 +0000 (Sat, 24 Jan 2009) | 2 lines

backport of 916228: fixed: looked better with the default margin, but the margin of the container layout is unwanted

------------------------------------------------------------------------
r916249 | kossebau | 2009-01-24 22:53:47 +0000 (Sat, 24 Jan 2009) | 2 lines

backport of 916247: changed: highlight Line Feed and Carriage Return

------------------------------------------------------------------------
r917156 | kossebau | 2009-01-26 23:52:31 +0000 (Mon, 26 Jan 2009) | 3 lines

backport of r917011: fixed: enable Copy button only if strings are selected (solution is workaround to at least Qt 4.4.3 error)


------------------------------------------------------------------------
r917912 | kossebau | 2009-01-28 19:50:31 +0000 (Wed, 28 Jan 2009) | 2 lines

backport of 917908: changed: use header->setResizeMode( QHeaderView::ResizeToContents ), better, but not perfect still with regard to column sizes

------------------------------------------------------------------------
r918223 | scripty | 2009-01-29 17:18:30 +0000 (Thu, 29 Jan 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r918367 | brandybuck | 2009-01-29 21:42:43 +0000 (Thu, 29 Jan 2009) | 4 lines

GUI:
FEATURE:
Backport from trunk.

------------------------------------------------------------------------
r918516 | metellius | 2009-01-30 01:42:51 +0000 (Fri, 30 Jan 2009) | 2 lines

Backporting revision 918515
BUG: 181678 quitOnLastWindowClosed property was causing the KApplication to quit when the replace dialog was opened. Also, set the KApplication to explicitly quit when the batch/add jobs emit their results.
------------------------------------------------------------------------
r919184 | darioandres | 2009-01-31 15:19:48 +0000 (Sat, 31 Jan 2009) | 6 lines

Use qulonglong instead of int to allow big partitions sizes to be detected properly 
Backport to 4.2branch of SVN commit 919161 by darioandres
 
BUG: 1335


------------------------------------------------------------------------
r919266 | darioandres | 2009-01-31 16:24:14 +0000 (Sat, 31 Jan 2009) | 6 lines

int->qulonglong for partitions sizes
4.2branch

BUG: 133588


------------------------------------------------------------------------
r919387 | brandybuck | 2009-01-31 21:09:45 +0000 (Sat, 31 Jan 2009) | 2 lines

backport fix for 181875

------------------------------------------------------------------------
r919488 | brandybuck | 2009-02-01 06:26:47 +0000 (Sun, 01 Feb 2009) | 4 lines

GUI:
FEATURE:
backport from trunk

------------------------------------------------------------------------
r919583 | mleupold | 2009-02-01 12:26:20 +0000 (Sun, 01 Feb 2009) | 5 lines

Backport of r919565 and r919566 to the 4.2 branch:
Allow whitespace characters in wallet names.

CCBUG:180357

------------------------------------------------------------------------
r921045 | darioandres | 2009-02-04 12:25:03 +0000 (Wed, 04 Feb 2009) | 6 lines

Don't blame the user on the first icon load (the _mount/_umount naming scheme is only needed when the icon is set by the user)
This is already fixed in my rework of KDF/KwikDisk for 4.3
Backporting the small fix to 4.2branch
BUG: 178824


------------------------------------------------------------------------
r921058 | darioandres | 2009-02-04 12:54:09 +0000 (Wed, 04 Feb 2009) | 6 lines

Force to not close on lastWindowClosed (as the whole app is a systray icon without parent widget)
Reorganized some of the QAction code to avoid lose the "Quit" action on a DFUpdate
Already fixed in 4.3 KDF version, backporting to 4.2branch
BUG: 167322


------------------------------------------------------------------------
r922556 | scripty | 2009-02-07 08:49:01 +0000 (Sat, 07 Feb 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r924602 | scripty | 2009-02-11 08:23:13 +0000 (Wed, 11 Feb 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r924884 | dakon | 2009-02-11 21:08:11 +0000 (Wed, 11 Feb 2009) | 1 line

remove GnuPG status message if encrypted text does not ent with \n
------------------------------------------------------------------------
r925802 | scripty | 2009-02-14 08:44:55 +0000 (Sat, 14 Feb 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r926771 | scripty | 2009-02-16 06:27:56 +0000 (Mon, 16 Feb 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r927740 | metellius | 2009-02-18 11:15:33 +0000 (Wed, 18 Feb 2009) | 2 lines

Backporting revision 927671
BUG: 175637 Applied patch from Volker Lanz to fix the session restoring in ark.
------------------------------------------------------------------------
r929993 | ilic | 2009-02-22 14:20:10 +0000 (Sun, 22 Feb 2009) | 1 line

i18n fixes (bport: 929991)
------------------------------------------------------------------------
r930230 | ilic | 2009-02-22 21:24:36 +0000 (Sun, 22 Feb 2009) | 1 line

i18n fixes (bport: 930228)
------------------------------------------------------------------------
r930809 | ilic | 2009-02-24 10:18:38 +0000 (Tue, 24 Feb 2009) | 1 line

Proper comment head on extraction. (bport: 930807)
------------------------------------------------------------------------
r931405 | scripty | 2009-02-25 09:02:16 +0000 (Wed, 25 Feb 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
